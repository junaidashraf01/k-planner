describe("Company page", () => {

    it(`we-talk-automotive-drupal-9`, function () {
        cy.visit('http://node.prod.ublox-website.ch4.amazee.io/en/blogs/we-talk-automotive');
        cy.wait(6000)

        // Call Open on eyes to initialize a test session
        cy.eyesOpen({
            appName: 'Story book',
            testName: 'we-talk-automotive-drupal-9',
        })
        cy.eyesCheckWindow({
            tag: "We-talk-automotive-drupal-9",
            target: 'window',
            fully: true
        });

        // Call Close on eyes to let the server know it should display the results
        cy.eyesClose()
    });

});