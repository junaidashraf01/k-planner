describe("Company page", () => {

    it(`we-talk-automotive-drupal-7`, function () {
        cy.visit('https://www.u-blox.com/en/we-talk-automotive');

        // Call Open on eyes to initialize a test session
        cy.eyesOpen({
            appName: 'Story book',
            testName: 'we-talk-automotive-drupal-7',
        })
        cy.eyesCheckWindow({
            tag: "We-talk-automotive-drupal-7",
            target: 'window',
            fully: true
        });

        // Call Close on eyes to let the server know it should display the results
        cy.eyesClose()
    });

});