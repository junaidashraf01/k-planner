/// <reference types = "cypress" />

// Test case: Visit application and validate login
it('Testing location content type', function(){
    cy.visit('https://nginx.prod.ublox-website.ch4.amazee.io')
    cy.get('#edit-name')
    cy.get('#edit-pass')
    cy.get('#edit-submit').click()
    cy.get('#edit-name').type('shhu')
    cy.get('#edit-pass').type('abcd@4321')
    cy.get('#edit-submit').click()

    // Test case:  add location content type
    cy.get('.toolbar-icon-system-admin-content').click({force: true})
    cy.get('.local-actions__item > .button').click()
    cy.get(':nth-child(10) > .admin-item__link').click()
    cy.get('#edit-field-office-types').select('Distributor')
    cy.get('#edit-field-sort-weight-0-value').type('1')
    cy.get('#edit-title-0-value').type('test location')
    cy.get('#edit-field-geofield-0-value-lat').type('31.520370')
    cy.get('#edit-field-geofield-0-value-lon').type('74.358749')
    cy.get('#edit-field-node-link-0-target-id').type('Stefan Berggren')
    cy.get('#edit-field-regions').select('Americas')
    cy.get('#edit-field-countries').select('Austria')
    cy.get('#edit-submit--2').click()

    // Test case: edit location content type
    cy.get('[data-original-order="1"] > .tabs__link').click()
    cy.get('#edit-field-office-types').select('Engineering Center')
    cy.get('#edit-field-sort-weight-0-value').type('2')
    cy.get('#edit-title-0-value').clear()
    cy.get('#edit-title-0-value').type('edit location')
    cy.get('#edit-field-geofield-0-value-lat').clear()
    cy.get('#edit-field-geofield-0-value-lat').type('32.520370')
    cy.get('#edit-field-geofield-0-value-lon').clear()
    cy.get('#edit-field-geofield-0-value-lon').type('75.358749')
    cy.get('#edit-field-node-link-0-target-id').clear()
    cy.get('#edit-field-node-link-0-target-id').type('Doris Rudischhauser')
    cy.get('#edit-field-regions').select('Africa')
    cy.get('#edit-field-countries').select('Belarus')
    cy.get('#edit-submit--2').click()
    cy.get('[data-original-order="1"] > .tabs__link').click()

    //Test case: delete location content type
     cy.get('[data-original-order="2"] > .tabs__link').click()
     cy.get('#edit-submit').click()

})