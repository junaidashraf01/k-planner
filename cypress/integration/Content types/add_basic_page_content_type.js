/// <reference types = "cypress" />


// Test case: Visit application and validate login
it('Testing basic page content type', function(){
    cy.visit('https://nginx.prod.ublox-website.ch4.amazee.io')
    cy.get('#edit-name')
    cy.get('#edit-pass')
    cy.get('#edit-submit').click()
    cy.get('#edit-name').type('shhu')
    cy.get('#edit-pass').type('abcd@4321')
    cy.get('#edit-submit').click()

    // Test case:  add basic page content type
    cy.get('.toolbar-icon-system-admin-content').click({force: true})
    cy.get('.local-actions__item > .button').click()
    cy.get(':nth-child(1) > .admin-item__link').click()
    cy.get('#edit-title-0-value').type('test basic page')
    cy.get('.cke_wysiwyg_frame').type('test body')
    cy.get('#edit-submit--2').click()

    // Test case: edit basic page content type
    cy.get('[data-original-order="1"] > .tabs__link').click()
    cy.get('#edit-title-0-value').clear()
    cy.get('#edit-title-0-value').type('u-blox basic page')
    cy.get('#edit-submit--2').click()
    cy.get('.js-active-tab > .tabs__link').click({force: true})
    cy.get('[data-original-order="1"] > .tabs__link').click()

    // Test case: delete basic page content type
     cy.get('[data-original-order="2"] > .tabs__link').click()
     cy.get('#edit-submit').click()

})