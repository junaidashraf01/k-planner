/// <reference types = "cypress" />

// Test case: Visit application and validate login
it('Testing company page content type', function(){
    cy.visit('https://nginx.prod.ublox-website.ch4.amazee.io')
    cy.get('#edit-name')
    cy.get('#edit-pass')
    cy.get('#edit-submit').click()
    cy.get('#edit-name').type('shhu')
    cy.get('#edit-pass').type('abcd@4321')
    cy.get('#edit-submit').click()

    // Test case:  add company page content type
    cy.get('.toolbar-icon-system-admin-content').click({force: true})
    cy.get('.local-actions__item > .button').click()
    cy.get(':nth-child(4) > .admin-item__link').click()
    cy.get('#edit-title-0-value').type('test company page')
    cy.get('#edit-field-image-open-button').click()
    cy.get(':nth-child(1) > .views-field-rendered-entity > .field-content > .media-library-item__preview-wrapper > .media-library-item__preview > .field > .image-style-medium').click({force: true})
    cy.get('.ui-dialog-buttonset > .media-library-select').click()
    cy.get('#edit-field-display-links-value').click({force: true})
    cy.get('#edit-field-invert-colour-value').click({force: true})
    cy.get('#edit-field-quick-information-label-0-value').type('test label')
    cy.get('#field-quick-information-content-reference-add-more').click({force: true})
    cy.get('#field-paragraphs-further-links-add-more').click({force: true})
    cy.get('#edit-submit--2').click()

    //Test case: edit company page content type
    cy.get(':nth-child(2) > .tabs__link').click()
    cy.get('#edit-title-0-value').clear()
    cy.get('#edit-title-0-value').type('company page')
    cy.get('#edit-field-invert-colour-value').click({force: true})
    cy.get('#edit-submit--2').click()
    cy.get('.js-active-tab > .tabs__link').click({force: true})

   // Test case: delete company page content type
     cy.get(':nth-child(3) > .tabs__link').click()
     cy.get('#edit-submit').click()

})