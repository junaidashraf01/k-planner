/// <reference types = "cypress" />

// Test case: Visit application and validate login
it('Testing webinar content type', function(){
    cy.visit('https://nginx.prod.ublox-website.ch4.amazee.io')
    cy.get('#edit-name')
    cy.get('#edit-pass')
    cy.get('#edit-submit').click()
    cy.get('#edit-name').type('shhu')
    cy.get('#edit-pass').type('abcd@4321')
    cy.get('#edit-submit').click()

    // Test case:  add webinar content type
    cy.get('.toolbar-icon-system-admin-content').click({force: true})
    cy.get('.local-actions__item > .button').click()
    cy.get(':nth-child(18) > .admin-item__link').click()
    cy.get('#edit-title-0-value').type('test webinar')
    cy.get('#field-paragraphs-author-add-more').click()
    cy.get('#edit-field-markets').select('Healthcare')
    cy.get('#edit-field-product-technologies').select('4G LTE')
    cy.get('#edit-field-megatrends').select('eHealth')
    cy.get('#edit-field-show-detail-page-value').click()
    cy.get('#edit-submit--2').click()

    // Test case: edit webinar content type
    cy.get(':nth-child(2) > .tabs__link').click()
    cy.get('#edit-title-0-value').clear()
    cy.get('#edit-title-0-value').type('edit webinar')
    cy.get('#edit-field-markets').select('Industry')
    cy.get('#edit-field-product-technologies').select('2G')
    cy.get('#edit-field-megatrends').select('Mobility')
    cy.get('#edit-field-show-detail-page-value').click()
    cy.get('#edit-submit--2').click()
    cy.get(':nth-child(2) > .tabs__link').click()

    // Test case: delete webinar content type
     cy.get(':nth-child(3) > .tabs__link').click()
     cy.get('#edit-submit').click()

})