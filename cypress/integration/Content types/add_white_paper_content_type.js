/// <reference types = "cypress" />

// Test case: Visit application and validate login
it('Testing white paper content type', function(){
    cy.visit('https://nginx.prod.ublox-website.ch4.amazee.io')
    cy.get('#edit-name')
    cy.get('#edit-pass')
    cy.get('#edit-submit').click()
    cy.get('#edit-name').type('shhu')
    cy.get('#edit-pass').type('abcd@4321')
    cy.get('#edit-submit').click()

    // Test case:  add white paper content type
    cy.get('.toolbar-icon-system-admin-content').click({force: true})
    cy.get('.local-actions__item > .button').click()
    cy.get(':nth-child(19) > .admin-item__link').click()
    cy.get('#edit-title-0-value').type('test whitepaper')
    cy.get('#edit-field-image-open-button').click()
    cy.get(':nth-child(1) > .views-field-rendered-entity > .field-content > .media-library-item__preview-wrapper > .media-library-item__preview > .field > .image-style-medium').click({force: true})
    cy.get('.ui-dialog-buttonset > .media-library-select').click()
    cy.get('#field-paragraphs-author-add-more').click()
    cy.get('#edit-field-markets').select('Healthcare')
    cy.get('#edit-field-product-technologies').select('4G LTE')
    cy.get('#edit-field-megatrends').select('eHealth')
    cy.get('#edit-submit--2').click()

    // Test case: edit white paper content type
    cy.get(':nth-child(2) > .tabs__link').click()
    cy.get('#edit-title-0-value').clear()
    cy.get('#edit-title-0-value').type('edit whitepaper')
    cy.get('#edit-field-markets').select('Industry')
    cy.get('#edit-field-product-technologies').select('2G')
    cy.get('#edit-field-megatrends').select('Mobility')
    cy.get('#edit-submit--2').click()
    cy.get(':nth-child(2) > .tabs__link').click()

    // Test case: delete white paper content type
     cy.get(':nth-child(3) > .tabs__link').click()
     cy.get('#edit-submit').click()

})