/// <reference types = "cypress" />

// Test case: Visit application and validate login
it('Testing product overview content type', function(){
    cy.visit('https://nginx.prod.ublox-website.ch4.amazee.io')
    cy.get('#edit-name')
    cy.get('#edit-pass')
    cy.get('#edit-submit').click()
    cy.get('#edit-name').type('shhu')
    cy.get('#edit-pass').type('abcd@4321')
    cy.get('#edit-submit').click()

    // Test case:  add product overview content type
    cy.get('.toolbar-icon-system-admin-content').click({force: true})
    cy.get('.local-actions__item > .button').click()
    cy.get(':nth-child(16) > .admin-item__link').click()
    cy.get('#edit-title-0-value').type('test product overview')
    cy.get('#edit-field-product-category-overview-value').click()
    cy.get('#edit-field-category-563').click()
    cy.get('#edit-submit--2').click()

    // Test case: edit product overview content type
    cy.get(':nth-child(2) > .tabs__link').click()
    cy.get('#edit-title-0-value').clear()
    cy.get('#edit-title-0-value').type('edit product overview')
    cy.get('#edit-field-product-category-overview-value').click()
    cy.get('#edit-field-category-743').click()
    cy.get('#edit-submit--2').click()
    cy.get(':nth-child(2) > .tabs__link').click()

    // Test case: delete product overview content type
     cy.get(':nth-child(3) > .tabs__link').click()
     cy.get('#edit-submit').click()

})