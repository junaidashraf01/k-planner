/// <reference types = "cypress" />


    // Test case: Visit application and validate login
    it('Testing document group content type', function(){
    cy.visit('https://nginx.prod.ublox-website.ch4.amazee.io')
    cy.get('#edit-name')
    cy.get('#edit-pass')
    cy.get('#edit-submit').click()
    cy.get('#edit-name').type('shhu')
    cy.get('#edit-pass').type('abcd@4321')
    cy.get('#edit-submit').click()

    // Test case:  add document group content type
    cy.get('.toolbar-icon-system-admin-content').click({force: true})
    cy.get('.local-actions__item > .button').click()
    cy.get(':nth-child(5) > .admin-item__link').click()
    cy.get('#edit-field-image-open-button').click()
    cy.get(':nth-child(1) > .views-field-rendered-entity > .field-content > .media-library-item__preview-wrapper > .media-library-item__preview > .field > .image-style-medium').click({force: true})
    cy.get('.ui-dialog-buttonset > .media-library-select').click()
    cy.get('#edit-title-0-value').type('test title',{ force: true })  
    cy.get('.cke_wysiwyg_frame').type('test subtitle',{ force: true })
    cy.get('#edit-field-date-0-value-date').click()
    cy.get('#edit-field-category').select('Reports')
    cy.get('#edit-field-order-hard-copy-value').click()
    cy.get('#edit-submit--2').click()

    // Test case: edit document group content type
    cy.get('[data-original-order="1"] > .tabs__link').click()
    cy.get('#edit-title-0-value').clear()
    cy.get('#edit-title-0-value').type('edit test title')
    cy.get('#edit-field-document-category').select('Presentations')
    cy.get('#edit-submit--2').click()

    // Test case: delete document group content type
    cy.get('[data-original-order="2"] > .tabs__link').click()
    cy.get('#edit-submit').click()
})