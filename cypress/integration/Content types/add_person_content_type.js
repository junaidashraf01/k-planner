/// <reference types = "cypress" />

// Test case: Visit application and validate login
it('Testing person content type', function(){
    cy.visit('https://nginx.prod.ublox-website.ch4.amazee.io')
    cy.get('#edit-name')
    cy.get('#edit-pass')
    cy.get('#edit-submit').click()
    cy.get('#edit-name').type('shhu')
    cy.get('#edit-pass').type('abcd@4321')
    cy.get('#edit-submit').click()

    // Test case:  add person content type
    cy.get('.toolbar-icon-system-admin-content').click({force: true})
    cy.get('.local-actions__item > .button').click()
    cy.get(':nth-child(14) > .admin-item__link').click()
    cy.get('#edit-field-first-name-0-value').type('test shahzad')
    cy.get('#edit-field-last-name-0-value').type('test hussain')
    cy.get('#edit-field-image-open-button').click()
    cy.get(':nth-child(1) > .views-field-rendered-entity > .field-content > .media-library-item__preview-wrapper > .media-library-item__preview > .field > .image-style-medium').click({force: true})
    cy.get('.ui-dialog-buttonset > .media-library-select').click()
    cy.get('#edit-field-phone-0-value').type('43434343')
    cy.get('#edit-field-fax-0-value').type('0989989')
    cy.get('#edit-field-link-0-uri').type('mailto:my.email@example.com')
    cy.get('#edit-field-twitter-0-uri').type('http://twitter.com')
    cy.get('#edit-field-facebook-0-uri').type('http://facebook.com')
    cy.get('#edit-field-linkedin-0-uri').type('http://linkedin.com')
    cy.get('#edit-field-skype-0-value').type('shahzad11')
    cy.get('#edit-field-person-categories-653').click({force: true})
    cy.get('#edit-field-name-of-contact-0-value').type('shahzad')
    cy.get('#edit-field-name-of-contact-2-0-value').type('hussain')
    cy.get('#edit-field-phone-2-0-value').type('4343434')
    cy.get('#edit-field-email-2-0-value').type('shah@test.com')
    cy.get('#edit-submit--2').click()

    // Test case: edit person content type
    // cy.get(':nth-child(2) > .tabs__link').click()
    // cy.get('#edit-title-0-value').clear()
    // cy.get('#edit-title-0-value').type('edit news')
    // cy.get('#edit-field-category').select('Press releases')
    // cy.get('#edit-submit--2').click()
    // cy.get(':nth-child(2) > .tabs__link').click()

   //  Test case: delete person content type
    //  cy.get(':nth-child(3) > .tabs__link').click()
    //  cy.get('#edit-submit').click()

})