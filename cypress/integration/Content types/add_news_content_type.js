/// <reference types = "cypress" />

// Test case: Visit application and validate login
it('Testing news content type', function(){
    cy.visit('https://nginx.prod.ublox-website.ch4.amazee.io')
    cy.get('#edit-name')
    cy.get('#edit-pass')
    cy.get('#edit-submit').click()
    cy.get('#edit-name').type('shhu')
    cy.get('#edit-pass').type('abcd@4321')
    cy.get('#edit-submit').click()

    // Test case:  add news content type
    cy.get('.toolbar-icon-system-admin-content').click({force: true})
    cy.get('.local-actions__item > .button').click()
    cy.get(':nth-child(12) > .admin-item__link').click()
    cy.get('#edit-title-0-value').type('test news')
    cy.get('#edit-field-category').select('Investor news')
    cy.get('#edit-submit--2').click()

    // Test case: edit news content type
    cy.get(':nth-child(2) > .tabs__link').click()
    cy.get('#edit-title-0-value').clear()
    cy.get('#edit-title-0-value').type('edit news')
    cy.get('#edit-field-category').select('Press releases')
    cy.get('#edit-submit--2').click()
    cy.get(':nth-child(2) > .tabs__link').click()

   //  Test case: delete news content type
     cy.get(':nth-child(3) > .tabs__link').click()
     cy.get('#edit-submit').click()

})