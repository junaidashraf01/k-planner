describe("Storybook", () => {

    it(`story book teaser grid five element`, function () {
        cy.visit('https://storybook.prod.ublox-website.ch4.amazee.io/iframe.html?id=components-teasergrid--five-elements&viewMode=story');

        // Call Open on eyes to initialize a test session
        cy.eyesOpen({
            appName: 'Story book',
            testName: 'story book teaser grid five element',
        })
        cy.eyesCheckWindow({
            tag: "Story book teaser grid five element",
            target: 'window',
            fully: true
        });

        // Call Close on eyes to let the server know it should display the results
        cy.eyesClose()
    });

});