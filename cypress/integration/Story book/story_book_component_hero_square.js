describe("Storybook", () => {

        it(`story book hero square`, function () {
            cy.visit('https://storybook.prod.ublox-website.ch4.amazee.io/iframe.html?id=components-hero--square&args=&viewMode=story');
    
            // Call Open on eyes to initialize a test session
            cy.eyesOpen({
                appName: 'Story book',
                testName: 'story book hero square',
            })
            cy.eyesCheckWindow({
                tag: "Story book hero square",
                target: 'window',
                fully: true
            });
    
            // Call Close on eyes to let the server know it should display the results
            cy.eyesClose()
        });
    
    });