describe("Storybook", () => {

    it(`story book large teaser image right`, function () {
        cy.visit('https://storybook.prod.ublox-website.ch4.amazee.io/iframe.html?id=components-largeteaser--image-right&viewMode=story');

        // Call Open on eyes to initialize a test session
        cy.eyesOpen({
            appName: 'Story book',
            testName: 'story book large teaser image right',
        })
        cy.eyesCheckWindow({
            tag: "Story book large teaser image right",
            target: 'window',
            fully: true
        });

        // Call Close on eyes to let the server know it should display the results
        cy.eyesClose()
    });

});