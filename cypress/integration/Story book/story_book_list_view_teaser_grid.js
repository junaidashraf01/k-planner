describe("Storybook", () => {

    it(`story book list view teaser grid`, function () {
        cy.visit('https://storybook.prod.ublox-website.ch4.amazee.io/iframe.html?id=components-listview--teaser-grid&viewMode=story');

        // Call Open on eyes to initialize a test session
        cy.eyesOpen({
            appName: 'Story book',
            testName: 'story book list view teaser grid',
        })
        cy.eyesCheckWindow({
            tag: "Story book list view teaser grid",
            target: 'window',
            fully: true
        });

        // Call Close on eyes to let the server know it should display the results
        cy.eyesClose()
    });

});