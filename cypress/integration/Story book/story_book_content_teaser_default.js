describe("StorybookContentTeaserdefault", () => {

    it(`story book content teaser default`, function () {
        cy.visit('https://storybook.prod.ublox-website.ch4.amazee.io/iframe.html?id=components-contentteaser--default&viewMode=story');

        // Call Open on eyes to initialize a test session
        cy.eyesOpen({
            appName: 'Story book',
            testName: 'content teaser default',
        })
        cy.eyesCheckWindow({
            tag: "Content teaser default",
            target: 'window',
            fully: true
        });

        // Call Close on eyes to let the server know it should display the results
        cy.eyesClose()
    });

});