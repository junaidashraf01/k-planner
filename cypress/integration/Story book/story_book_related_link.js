describe("StorybookRelatedlink", () => {

    it(`story book related link`, function () {
        cy.visit('http://storybook.prod.ublox-website.ch4.amazee.io/iframe.html?id=components-relatedlinks--default&viewMode=story');

        // Call Open on eyes to initialize a test session
        cy.eyesOpen({
            appName: 'Story book',
            testName: 'related link',
        })
        cy.eyesCheckWindow({
            tag: "Related link",
            target: 'window',
            fully: true
        });

        // Call Close on eyes to let the server know it should display the results
        cy.eyesClose()
    });

});