describe("Storybook", () => {

        it(`story book slider`, function () {
            cy.visit('https://storybook.prod.ublox-website.ch4.amazee.io/iframe.html?id=components-slider--simple&args=&viewMode=story');
    
            // Call Open on eyes to initialize a test session
            cy.eyesOpen({
                appName: 'Story book',
                testName: 'story book slider',
            })
            cy.eyesCheckWindow({
                tag: "Story book slider",
                target: 'window',
                fully: true
            });
    
            // Call Close on eyes to let the server know it should display the results
            cy.eyesClose()
        });
    
    });