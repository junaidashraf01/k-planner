describe("Storybook", () => {

        it(`story book component hero hexagon`, function () {
            cy.visit('https://storybook.prod.ublox-website.ch4.amazee.io/iframe.html?id=components-hero--hexagon&args=&viewMode=story');
    
            // Call Open on eyes to initialize a test session
            cy.eyesOpen({
                appName: 'Story book',
                testName: 'story book component hero hexagon',
            })
            cy.eyesCheckWindow({
                tag: "Story book component hero hexagon",
                target: 'window',
                fully: true
            });
    
            // Call Close on eyes to let the server know it should display the results
            cy.eyesClose()
        });
    
    });