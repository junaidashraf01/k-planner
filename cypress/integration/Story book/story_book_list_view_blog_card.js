describe("Storybook", () => {

    it(`story book list view blog card`, function () {
        cy.visit('https://storybook.prod.ublox-website.ch4.amazee.io/iframe.html?id=components-listview--blog-card&viewMode=story');

        // Call Open on eyes to initialize a test session
        cy.eyesOpen({
            appName: 'Story book',
            testName: 'story book list view blog card',
        })
        cy.eyesCheckWindow({
            tag: "Story book list view blog card",
            target: 'window',
            fully: true
        });

        // Call Close on eyes to let the server know it should display the results
        cy.eyesClose()
    });

});