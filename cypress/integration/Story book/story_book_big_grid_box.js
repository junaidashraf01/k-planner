describe("StorybookBigGridbox", () => {

    it(`story book big grid box`, function () {
        cy.visit('https://storybook.prod.ublox-website.ch4.amazee.io/iframe.html?id=components-biggridbox--two-elements&viewMode=story');

        // Call Open on eyes to initialize a test session
        cy.eyesOpen({
            appName: 'Story book',
            testName: 'big grid box',
        })
        cy.eyesCheckWindow({
            tag: "Big grid box",
            target: 'window',
            fully: true
        });

        // Call Close on eyes to let the server know it should display the results
        cy.eyesClose()
    });

});