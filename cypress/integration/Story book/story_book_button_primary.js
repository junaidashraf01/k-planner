describe("StorybookPrimarybutton", () => {

    it(`story book button primary`, function () {
        cy.visit('https://storybook.prod.ublox-website.ch4.amazee.io/iframe.html?id=components-button--primary&viewMode=story');

        // Call Open on eyes to initialize a test session
        cy.eyesOpen({
            appName: 'Story book',
            testName: 'primary button',
        })
        cy.eyesCheckWindow({
            tag: "Primary button window",
            target: 'window',
            fully: true
        });

        // Call Close on eyes to let the server know it should display the results
        cy.eyesClose()
    });

});