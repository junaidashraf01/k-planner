describe("Storybook", () => {

    it(`story book teaser grid three element`, function () {
        cy.visit('https://storybook.prod.ublox-website.ch4.amazee.io/iframe.html?id=components-teasergrid--three-elements&viewMode=story');

        // Call Open on eyes to initialize a test session
        cy.eyesOpen({
            appName: 'Story book',
            testName: 'story book teaser grid three element',
        })
        cy.eyesCheckWindow({
            tag: "Story book teaser grid three element",
            target: 'window',
            fully: true
        });

        // Call Close on eyes to let the server know it should display the results
        cy.eyesClose()
    });

});