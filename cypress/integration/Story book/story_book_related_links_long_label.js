describe("Storybook", () => {

    it(`story book related links long label`, function () {
        cy.visit('https://storybook.prod.ublox-website.ch4.amazee.io/iframe.html?id=components-relatedlinks--long-label&viewMode=story');

        // Call Open on eyes to initialize a test session
        cy.eyesOpen({
            appName: 'Story book',
            testName: 'story book related links long label',
        })
        cy.eyesCheckWindow({
            tag: "Story book related links long label",
            target: 'window',
            fully: true
        });

        // Call Close on eyes to let the server know it should display the results
        cy.eyesClose()
    });

});