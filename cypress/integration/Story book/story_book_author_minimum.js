describe("StorybookAuthorminimum", () => {

    it(`story book author minimum`, function () {
        cy.visit('https://storybook.prod.ublox-website.ch4.amazee.io/iframe.html?id=components-author--minimal&viewMode=story');

        // Call Open on eyes to initialize a test session
        cy.eyesOpen({
            appName: 'Story book',
            testName: 'author minimum',
        })
        cy.eyesCheckWindow({
            tag: "Author minimum window",
            target: 'window',
            fully: true
        });

        // Call Close on eyes to let the server know it should display the results
        cy.eyesClose()
    });

});