describe("Storybook", () => {

        it(`story book home hero with video`, function () {
            cy.visit('https://storybook.prod.ublox-website.ch4.amazee.io/iframe.html?id=components-homehero--with-video&args=&viewMode=story');
    
            // Call Open on eyes to initialize a test session
            cy.eyesOpen({
                appName: 'Story book',
                testName: 'story book home hero with video',
            })
            cy.eyesCheckWindow({
                tag: "Story book home hero with video",
                target: 'window',
                fully: true
            });
    
            // Call Close on eyes to let the server know it should display the results
            cy.eyesClose()
        });
    
    });