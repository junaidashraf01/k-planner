context('Product page', () => {
    it('takes a screenshot', () => {
        cy.visit('https://storybook.prod.ublox-website.ch4.amazee.io/iframe.html?id=components-largeteaser--image-left&viewMode=story')
        cy.wait(6000)
        cy.screenshot()
    })
  })