describe("Storybook", () => {

    it(`story book quicklinks four element`, function () {
        cy.visit('https://storybook.prod.ublox-website.ch4.amazee.io/iframe.html?id=components-quicklinks--four-elements&viewMode=story');

        // Call Open on eyes to initialize a test session
        cy.eyesOpen({
            appName: 'Story book',
            testName: 'story book quicklinks four element',
        })
        cy.eyesCheckWindow({
            tag: "Story book quicklinks four element",
            target: 'window',
            fully: true
        });

        // Call Close on eyes to let the server know it should display the results
        cy.eyesClose()
    });

});