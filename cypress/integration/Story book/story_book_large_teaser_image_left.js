describe("Storybook", () => {

    it(`story book large teaser image left`, function () {
        cy.visit('https://storybook.prod.ublox-website.ch4.amazee.io/iframe.html?id=components-largeteaser--image-left&viewMode=story');

        // Call Open on eyes to initialize a test session
        cy.eyesOpen({
            appName: 'Story book',
            testName: 'story book large teaser image left',
        })
        cy.eyesCheckWindow({
            tag: "Story book large teaser image left",
            target: 'window',
            fully: true
        });

        // Call Close on eyes to let the server know it should display the results
        cy.eyesClose()
    });

});