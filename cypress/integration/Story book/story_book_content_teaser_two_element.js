describe("StorybookContentTeaserTwoelement", () => {

    it(`story book content teaser two element`, function () {
        cy.visit('https://storybook.prod.ublox-website.ch4.amazee.io/iframe.html?id=components-contentteaser--two-elements&viewMode=story');

        // Call Open on eyes to initialize a test session
        cy.eyesOpen({
            appName: 'Story book',
            testName: 'content teaser two element',
        })
        cy.eyesCheckWindow({
            tag: "Content teaser two element",
            target: 'window',
            fully: true
        });

        // Call Close on eyes to let the server know it should display the results
        cy.eyesClose()
    });

});