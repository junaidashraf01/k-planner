describe("Storybook", () => {

        it(`story book dual slider electrical data`, function () {
            cy.visit('https://storybook.prod.ublox-website.ch4.amazee.io/iframe.html?id=search-dualsliderfilterpill--electrical-data&args=&viewMode=story');
    
            // Call Open on eyes to initialize a test session
            cy.eyesOpen({
                appName: 'Story book',
                testName: 'story book dual slider electrical data',
            })
            cy.eyesCheckWindow({
                tag: "Story book dual slider electrical data",
                target: 'window',
                fully: true
            });
    
            // Call Close on eyes to let the server know it should display the results
            cy.eyesClose()
        });
    
    });