describe("StorybookGridBoxelements", () => {

    it(`story book grid box elements`, function () {
        cy.visit('http://storybook.prod.ublox-website.ch4.amazee.io/iframe.html?id=components-gridbox--five-elements&viewMode=story');

        // Call Open on eyes to initialize a test session
        cy.eyesOpen({
            appName: 'Story book',
            testName: 'grid box elements',
        })
        cy.eyesCheckWindow({
            tag: "Grid box elements",
            target: 'window',
            fully: true
        });

        // Call Close on eyes to let the server know it should display the results
        cy.eyesClose()
    });

});