describe("StorybookSecondarybutton", () => {

    it(`story book button secondary`, function () {
        cy.visit('https://storybook.prod.ublox-website.ch4.amazee.io/iframe.html?id=components-button--secondary&viewMode=story');

        // Call Open on eyes to initialize a test session
        cy.eyesOpen({
            appName: 'Story book',
            testName: 'secondary button',
        })
        cy.eyesCheckWindow({
            tag: "Secondary button window",
            target: 'window',
            fully: true
        });

        // Call Close on eyes to let the server know it should display the results
        cy.eyesClose()
    });

});